package Restart;

import java.util.Scanner;

public class Switches {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		String mes = sc.nextLine();
		int mesNum=0;
		
		//les string no es comparen amb el == sino amb l'equals
		//equals -> case sensitive   equalsIgnoreCase -> case insensitive
		/*
		if(mes.equalsIgnoreCase("gener")) {
			mesNum=1;
		}else if (mes.equalsIgnoreCase("febrer")) {
			mesNum=2;
		}else if (mes.equalsIgnoreCase("març")) {
			mesNum=3;
		}else if (mes.equalsIgnoreCase("abril")) {
			mesNum=4;
		}else if (mes.equalsIgnoreCase("maig")) {
			mesNum=5;
		}else if (mes.equalsIgnoreCase("juny")) {
			mesNum=6;
		}else if (mes.equalsIgnoreCase("juliol")) {
			mesNum=7;
		}else if (mes.equalsIgnoreCase("agost")) {
			mesNum=8;
		}else if (mes.equalsIgnoreCase("setembre")) {
			mesNum=9;
		}else if (mes.equalsIgnoreCase("octubre")) {
			mesNum=10;
		}else if (mes.equalsIgnoreCase("novembre")) {
			mesNum=11;
		}else if (mes.equalsIgnoreCase("desembre")) {
			mesNum=12;
		}else {
			System.out.println("mes incorrecte");
		}
		
		
		*/
		switch(mes) {
		case "gener":
			mesNum=1;
			//break;
		case "febrer":
			mesNum=2;
			break;
		case "març":
			mesNum=3;
			break;
		case "abril":
			mesNum=4;
			break;
		case "maig":
			mesNum=5;
			break;
		case "juny":
			mesNum=6;
			break;
		case "juliol":
			mesNum=7;
			break;
		case "agost":
			mesNum=8;
			break;
		case "setembre":
			mesNum=9;
			break;
		case "octubre":
			mesNum=10;
			break;
		case "novembre":
			mesNum=11;
			break;
		case "desembre":
			mesNum=12;
			break;
		default:
			mesNum = -1;
			System.out.println("error");
			break;
		}
		System.out.println(mesNum);
		
		
	}

}
