package Restart;

import java.util.Scanner;

public class VidaYMilagrosDeNuestroSenyorScanner {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		//System.out.println("aaaa\\n\tbbbbb\n\r \ncccccc");
		
		
		//nextLine, llegeix la linea
		//Que significa linea?
		//busca un Line Feed.   \n  <--- salt de linea
		String linea = sc.nextLine();
		System.out.println(linea);
		//next, llegeix el token
		//el token esta separat per espais, salts de linea, tabulacions, etc
		String token = sc.next();
		System.out.println(token);
		int a = sc.nextInt();
		System.out.println(a+2);
		
		
		
		/**
		 * NORMA UNIVERSAL DEL SCANNER
		 * 
		 * SI TENS SC.NEXTINT() I JUST DESPRES SC.NEXTLINE()
		 * SI HAS DE LLEGIR NUMEROS I DESPRES TEXT
		 * HAS DE FER UN NEXTLINE BUIT ENTREMIG
		 */
		
		
		
		
		
		
	}

}
