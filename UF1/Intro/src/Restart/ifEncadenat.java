package Restart;

public class ifEncadenat {
	
	
	public static void main(String[] args) {
		
		
		///declaracio multiple
		boolean a,b,c,d,e;
		a=false;
		b=true;
		c=true;
		d=false;
		e=true;
		
		
		if(a==true) {
			System.out.println("a");
		}
		
		if(a) {
			System.out.println("b");
		}
		
		
		if(a) {
			if(b) {
				System.out.println("a&&b");
			}
		}else {
			if(b) {
				System.out.println("!a&&b");
			}else {
				System.out.println("!(a||b)");
			}
		}
		
		
		if(a) {  //A
			if(b) {  //B
				System.out.println("poma");
			}else if(c) {  //B
				System.out.println("pera");
			}else { //B
				System.out.println("taronja");
			}
			
		}else if (b) {  //A
			if(c&&d) {  //C
				System.out.println("platan");
			}else if (c||d) {  //C
				System.out.println("mango");
			}if(d) {  //D
				System.out.println("aguacate");
			}else {  //D
				System.out.println("maracuya");
			}
		}else {  //A
			if(c) {  //E
				if(d) {  //F
					if(e) {  //G
						System.out.println("piña");
					}
				}
			}
		}
		
	}

}
