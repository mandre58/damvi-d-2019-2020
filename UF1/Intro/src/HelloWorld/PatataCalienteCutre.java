package HelloWorld;

import java.util.Random;
import java.util.Scanner;

public class PatataCalienteCutre {
	
	
	public static void main(String[] args) {
		
		//Declarar totes les variables al principi
		///ZONA DE DECLARACIONS
		Random r = new Random();
		//genera un numero aleatori del 0 al 99 i suma 1 (queda un numero aleatori del 1 al 100)
		int resultat = r.nextInt(100)+1;
		Scanner sc = new Scanner(System.in);
		int resposta = -1;
		int contador = -5;
		///ZONA DE DECLARACIONS
		
		while(resposta!=resultat && contador<5) {
			System.out.println("intenta endevinar l'edat del teu professor preferit ");
			
			//llegeix la resposta per escanner
			resposta = sc.nextInt();
			
			//si la resposta es mes gran que el resultat
			if(resposta>resultat) {
				System.out.println("menos");
				contador++;
			//si NO es lo anterior i la resposta es mes petita que el resultat
			}else if (resposta < resultat) {
				System.out.println("mas");
				contador++;
			//si NO es cap de les dues anteriors i la resposta es igual al resultat
			}else if (resposta == resultat) {
				System.out.println("correcta");
			}
			
			//if separat. Es separat perque el volem comprovar sempre.
			if(contador==5) {
				System.out.println("you died, la respuesta correcta es "+resultat);
			}
			
		}
		
		System.out.println("gracies per jugar");
		
		
		
		
	}

}
