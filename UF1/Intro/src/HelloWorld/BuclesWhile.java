package HelloWorld;

public class BuclesWhile {

	
	public static void main(String[] args) {
		
		int contador = 0;
		
		while(contador<100) {
			contador++;
			System.out.println(contador);
		}
		
		
		int a = 4;
		int b = 6;
		int c = 10;
		int d = 389492013;
		
		
		///   Or:   boolea  ||  boolea  . Torna true si una de les dues condicions es true
		///   And:  boolea  &&  boolea  . Torna true si les dues condicions son true;
		
		System.out.println(a<b); //true
		System.out.println(d<c); //false
		System.out.println(a<b || d<c); //true
		System.out.println(a<b && d<c); //true
		
		if(a<b || d<c) {
			System.out.println("hola");
		}else {
			System.out.println("adeu");
		}
			
		
		
		
	}
}
