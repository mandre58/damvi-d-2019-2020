package HelloWorld;

public class HelloWorld {
	
	
	//cualquier cosa que empiece con doble barra es un comentario
	
	/*
	 * En un lugar de la mancha
	 * de cuyo nombre no quiero acordarme
	 * habia un hidalgo con lanza y algarba y otras cosas
	 * 
	 */
	
	//Esta funcion es la que se llama al principio de todo siempre 
	//cuando ejecutas el programa
	public static void main(String[] args) {
		
		//int: Numero entero (sin decimales)
		int numero = 0;
		int var = 4;
		//les variables poden adoptar valors
		//que depenen d'altres variables
		int var2 = numero/var;
		
		System.out.println("El resultat de numero/var es igual a: "+var2);
		
		//double: Nombre amb decimals
		
		//no poden haver dos variables amb el mateix nom;
		double numeroDecimal = 0;
		
		numeroDecimal += 4.3;
		/// a += 3;  es el mateix que  a = a+3;
		
		//++ suma 1.
		numeroDecimal++;
		//-- resta 1.
		numeroDecimal--;
		
		System.out.println("El resultat de sumar 1 i restar 1 es... "+numeroDecimal);
		
		
		//String. Cadena de caracters
		
		String nom = "Raul";
		String cognom = "Ruiz";
		
		//+ concatena strings
		String nomComplet = nom+" "+cognom;
		
		System.out.println(nomComplet);
		
		
		///boolean : true o false
		
		boolean valor = true;
		
		valor = false;
		
		
		//negaci�: !
		
		valor = !valor;
		
		System.out.println("el boole� �s: "+valor);
		
		///condicials:
		//if(condicio_booleana){
			//codi si es compleix la condicio
		//}else{
			//codi si NO es compleix la condici�
		//}
		
		valor = false;
		if(valor) {
			System.out.println("El valor es true");
		}else {
			System.out.println("El valor es fals");
		}
		
		
		
		
		//declaracion
		//TIPO NOMBRE = VALOR_INCIAL;
		
		System.out.println("Hola Damvi");
		
		
		
		
		
	}
	
	

}
