package HelloWorld;

import java.util.Scanner;

public class Input {
	
	public static void main(String[] args) {
		
		///classe d'output estandar
		System.out.println("Hola mundo!!! (otra vez)");
		
		//DECLARAR
		///TIPUS NOM = VALOR_INICIAL
		Scanner scanner = new Scanner(System.in);

		//Com funciona l'escanner
		
		System.out.println("Com et dius?");
		String nom = scanner.nextLine();
		System.out.println("Hola "+nom+" digues com estas");
		String moltBe = scanner.nextLine();
		System.out.println("i les teves amistats com van");
		
		
		//Scanner de nombres
		
		System.out.println("Introdueix un nombre");
		int nombre = scanner.nextInt();
		System.out.println("Introdueix un altre nombre");
		int altreNombre = scanner.nextInt();
		
		int suma = nombre + altreNombre;
		
		System.out.println("la suma es "+suma);
		
		if(suma>=8) {
			System.out.println("el tama�o importa");
		}else {
			System.out.println("en el pot petit est� la bona confitura");
		}
		
		
		
		
	}

}
