package HelloWorld;

import java.util.Scanner;

public class MultiplicarPer3 {
	

	//qualsevol funci� que no sigui main no ser� executada a no ser que la cridem nosaltrres
	public static void no_main() {
		
		System.out.println("C");
	}

	//El programa busca la funci� main i l'executa la primera de totes
	public static void main(String[] args) {
		//dintre d'una funci� les instruccions s'executen en ordre (la que esta m�s amunt s'executa primer)
		System.out.println("A");
		System.out.println("B");
		
		/**
		 * Generalment es una bona idea comen�ar el programa
		 * pensant quines variables necessites i has de declarar
		 */
	
		/**
		 * tipus d'instruccions de java que veureu de moment
		 * 
		 * 1. Declaraci�
		 * TIPUS nom_variable [= VALOR_INICIAL]; (els claudators signifiquen que �s opcional)
		 * 
		 * 2. Assignacions
		 * nom_variable_ja_declarada = NOU_VALOR;
		 * 
		 * 3. if/while/for/switch  Instruccions de Control
		 * instruccio( condici�_booleana ) {
		 * 	la clau s'haur� de tancar al final del bloc de control
		 * }
		 * 
		 * 4. Funcions void
		 * 
		 * funcio( par�metres_de_la_funcio) ;
		 * 
		 * de moment nom�s coneixem el Syso
		 * 
		 * 
		 * 
		 * 
		 */
		
		
		//DECLARACIONS
		//Scanner. EN TOT EL PROGRAMA NOMES HI HA UN.
		Scanner sc = new Scanner(System.in); //declaracio
		int numero;  //declaracio
		int resultat;  //declaracio
		
		
		
		//llegeix un numero per teclat i el posem a numero
		numero = sc.nextInt();  //assignaci�
		
		//el multipliquem per 3
		resultat = numero * 3; //assignaci�
		
		//imprimim per pantalla
		System.out.println(resultat);  //void
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
