package HelloWorld;

public class Comparacions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*Operacions de comparacio
		 * TOTES LES OPERACIONS DE COMPARACIO TORNEN
		 * BOOLEANS
		 * == igualtat
		 * != diferencia
		 * < menor que
		 * > major que
		 * <= menor o igual que
		 * >= major o igual que
		 * ^ COSES RARES QUE NO ENTEN NINGU
		 */
		
		int a = 5;
		int b = 5;
		System.out.println(a!=b);
		
		if(a<b) {
			System.out.println("a es mes petit que b");
		}else if(b<a) {
			System.out.println("b es mes petit que a");
		//si no es compleix CAP de les condiciones anteriors
		}else {
			System.out.println("a i b son iguals");
		}
		
		
		
		///COMENTARI DE CANVI

	}

}
